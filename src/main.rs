use gstreamer as gst;
use gst::prelude::*;

fn main() {
    gst::init().unwrap();

    let registry = gst::Registry::get();
    let compositor = registry.find_plugin("compositor").unwrap();

    println!("Hello, world!");
}
